// ============================================================================
//
// Copyright (C) 2006-2014 Talend Inc. - www.talend.com
//
// This source code is available under agreement available at
// %InstallDIR%\features\org.talend.rcp.branding.%PRODUCTNAME%\%PRODUCTNAME%license.txt
//
// You should have received a copy of the agreement
// along with this program; if not, write to Talend SA
// 9 rue Pages 92150 Suresnes, France
//
// ============================================================================
package org.talend.plugin.deps;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.talend.plugin.deps.model.Bundle;

/**
 * created by sgandon on 28 juil. 2014 Detailled comment
 *
 */
public class Main {

    /**
     * 
     */
    private static final String NO_EXTERNALS_OPTION = "no_externals"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String GRAPHVIZ_FORMAT_OPTION = "graphviz"; //$NON-NLS-1$

    // private static final String ROOT_OPTION = "root"
    /**
     * DOC sgandon Comment method "main".
     * 
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Options options = createOptions();
        // create the parser
        CommandLineParser parser = new PosixParser();
        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);
            String[] filesToProcess = line.getArgs();
            if (filesToProcess.length != 1) {
                throw new ParseException("missing (or too much) file argument(s).");
            }
            ManifestDependencyParser manifestDependencyParser = new ManifestDependencyParser(new File(filesToProcess[0]));
            manifestDependencyParser.parse();
            if (line.hasOption(GRAPHVIZ_FORMAT_OPTION)) {// create a graph viz file
                graphVizDump(manifestDependencyParser, filesToProcess[0], line.hasOption(NO_EXTERNALS_OPTION));
            } else {// output the bundle list and dependencies with their location
                simpleDump(manifestDependencyParser);
            }
        } catch (ParseException exp) {
            // oops, something went wrong
            System.err.println("Parsing failed.  Reason: " + exp.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("ant", options);
        }
    }

    /**
     * DOC sgandon Comment method "graphVizDump".
     * 
     * @param manifestDependencyParser
     * @param rootPath
     * @param noExternalBundle
     * @throws IOException
     */
    private static void graphVizDump(ManifestDependencyParser manifestDependencyParser, String rootPath, boolean noExternalBundle)
            throws IOException {
        LinkedList<String> nodesGraphs = new LinkedList<>();
        generateNodeGraphStr(manifestDependencyParser, nodesGraphs, noExternalBundle);
        LinkedList<String> subGraph = new LinkedList<>();
        // generatedSubGraphStr(manifestDependencyParser, rootPath, subGraph);
        Writer writer = new OutputStreamWriter(System.out);
        try {
            writer.append("digraph features {\n    size=\"6,6\";\n    node [color=lightblue2, style=filled, shape=Box];");

            for (String aLine : nodesGraphs) {
                writer.append("\n" + aLine);
            }
            for (String aLine : subGraph) {
                writer.append("\n" + aLine);
            }
            writer.append("\n}\n");
            writer.flush();
        } finally {
            writer.close();
        }
    }

    /**
     * DOC sgandon Comment method "generatedSubGraphStr".
     * 
     * @param rootPath
     * 
     * @param manifestDependencyParser
     * 
     * @param subGraph
     */
    static void generatedSubGraphStr(ManifestDependencyParser mdp, String rootPathS, LinkedList<String> subGraph) {
        HashMap<String, Set<String>> clusterMap = new HashMap<>();// location/ set of bundle symbolic name
        computeClusterMap(mdp, rootPathS, clusterMap);
        int clusterNb = 0;
        for (String clusterName : clusterMap.keySet()) {
            subGraph.add("subgraph cluster" + clusterNb++ + "{");
            Set<String> bsnSet = clusterMap.get(clusterName);
            try {
                for (String bsn : bsnSet) {
                    subGraph.add('"' + bsn + '"' + ";");
                }
            } finally {
                subGraph.add("}");
            }
        }
    }

    /**
     * DOC sgandon Comment method "computeClusterMap".
     * 
     * @param mdp
     * @param rootPathS
     * @param clusterMap
     */
    private static void computeClusterMap(ManifestDependencyParser mdp, String rootPathS, HashMap<String, Set<String>> clusterMap) {
        Path rootPath = FileSystems.getDefault().getPath(rootPathS);
        for (Bundle bundle : mdp.getAllBundles()) {
            Path bundlePath = FileSystems.getDefault().getPath(bundle.getBundlePath());
            Path rootRelativeBundlePath = bundlePath.subpath(rootPath.getNameCount(), bundlePath.getNameCount() - 1);
            String clusterName = rootRelativeBundlePath.subpath(0, 1).toString();
            Set<String> clusterBsnSet;
            if (!clusterMap.containsKey(clusterName)) {
                clusterBsnSet = new HashSet<>();
                clusterMap.put(clusterName, clusterBsnSet);
            } else {
                clusterBsnSet = clusterMap.get(clusterName);
            }
            clusterBsnSet.add(bundle.getBsn());
        }
    }

    /**
     * DOC sgandon Comment method "generateNodeGraphStr".
     * 
     * @param noExternalBundle
     * 
     * @param manifestDependencyParser
     * 
     * @param nodesGraphsStr
     */
    private static void generateNodeGraphStr(ManifestDependencyParser mdp, LinkedList<String> nodesGraphs, boolean noExternalDeps) {
        Map<String, Bundle> bsnBundlesMap = mdp.getBsnBundlesMap();
        for (Bundle bundle : bsnBundlesMap.values()) {
            for (String reqBsn : bundle.getRequiredBundles()) {
                String bundleBsn = bundle.getBsn();
                if (!noExternalDeps || bsnBundlesMap.containsKey(reqBsn)) {
                    nodesGraphs.add('"' + bundleBsn + '"' + " -> " + '"' + reqBsn + '"' + ";");
                }
            }
        }
    }

    /**
     * DOC sgandon Comment method "simpleDump".
     * 
     * @param manifestDependencyParser
     */
    static void simpleDump(ManifestDependencyParser manifestDependencyParser) {
        Collection<Bundle> allBundles = manifestDependencyParser.getAllBundles();
        for (Bundle bundle : allBundles) {
            System.out.println(bundle.getBsn() + ":" + bundle.getBundlePath()); //$NON-NLS-1$
            System.out.println("  required bundles:"); //$NON-NLS-1$
            for (String reqBundle : bundle.getRequiredBundles()) {
                System.out.println("  " + reqBundle);
            }
        }

    }

    /**
     * DOC sgandon Comment method "createOptions".
     * 
     * @return
     */
    private static Options createOptions() {
        Option graphVizFileOption = OptionBuilder
                .withDescription("output in the GraphViz file format").create(GRAPHVIZ_FORMAT_OPTION); //$NON-NLS-1$
        Option noExternalsOption = OptionBuilder
                .withDescription("Remove any reference to external bundles").create(NO_EXTERNALS_OPTION); //$NON-NLS-1$
        // Option replaceOption = OptionBuilder.isRequired().withArgName("replace expression").hasArgs(1)
        // .withDescription("the replace string expression").create(REPLACE_OPTION);
        // Option fileOption = OptionBuilder.isRequired().withDescription("the replace string expression").create();
        Options options = new Options();
        options.addOption(graphVizFileOption);
        options.addOption(noExternalsOption);
        // options.addOption(replaceOption);
        // options.addOption(fileOption);
        return options;
    }

}
