// ============================================================================
//
// Copyright (C) 2006-2014 Talend Inc. - www.talend.com
//
// This source code is available under agreement available at
// %InstallDIR%\features\org.talend.rcp.branding.%PRODUCTNAME%\%PRODUCTNAME%license.txt
//
// You should have received a copy of the agreement
// along with this program; if not, write to Talend SA
// 9 rue Pages 92150 Suresnes, France
//
// ============================================================================
package org.talend.plugin.deps.model;

import java.util.jar.Attributes;

/**
 * created by sgandon on 28 juil. 2014 Detailled comment
 *
 */
public class Bundle {

    private String bsn;// Bundle Symbolic Name

    private String[] requiredBundles;

    private String bundlePath;

    /**
     * DOC sgandon Bundle constructor comment.
     * 
     * @param bsn
     * @param bundlePath
     */
    public Bundle(String bsn, String bundlePath) {
        this.bsn = bsn;
        this.bundlePath = bundlePath;
    }

    public void parse(Attributes attributes) {
        String requiredBundlesS = attributes.getValue("Require-Bundle"); //$NON-NLS-1$
        if (requiredBundlesS != null) {
            requiredBundles = requiredBundlesS.split(","); //$NON-NLS-1$
        } else {
            requiredBundles = new String[0];
        }
        cleanRequiredBundles();
    }

    /**
     * removes everything behind the first semi-colon if the requiredBundles
     */
    private void cleanRequiredBundles() {
        for (int i = 0; i < requiredBundles.length; i++) {
            int semiColonId = requiredBundles[i].indexOf(';');
            if (semiColonId != -1) {
                requiredBundles[i] = requiredBundles[i].substring(0, semiColonId);
            }
        }

    }

    /**
     * Getter for requiredBundles.
     * 
     * @return the requiredBundles
     */
    public String[] getRequiredBundles() {
        return requiredBundles;
    }

    /**
     * Getter for bsn.
     * 
     * @return the bsn
     */
    public String getBsn() {
        return bsn;
    }

    /**
     * Getter for bundlePath.
     * 
     * @return the bundlePath
     */
    public String getBundlePath() {
        return bundlePath;
    }

}
