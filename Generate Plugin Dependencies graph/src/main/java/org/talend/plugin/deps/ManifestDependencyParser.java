// ============================================================================
//
// Copyright (C) 2006-2014 Talend Inc. - www.talend.com
//
// This source code is available under agreement available at
// %InstallDIR%\features\org.talend.rcp.branding.%PRODUCTNAME%\%PRODUCTNAME%license.txt
//
// You should have received a copy of the agreement
// along with this program; if not, write to Talend SA
// 9 rue Pages 92150 Suresnes, France
//
// ============================================================================
package org.talend.plugin.deps;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.jar.Manifest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.talend.plugin.deps.model.Bundle;

/**
 * created by sgandon on 28 juil. 2014 Detailled comment
 *
 */
public class ManifestDependencyParser {

    private File root;

    private Map<String, Bundle> bundlesMap;// map of bsn/Bundle (not used as a map but who knows

    /**
     * DOC sgandon ManifestDependencyParser constructor comment.
     * 
     * @param string
     */
    public ManifestDependencyParser(File root) {
        this.root = root;
    }

    public void parse() throws IOException {
        Map<String, Manifest> locationAndManifests = getAllManifests();
        computeAllBundles(locationAndManifests);
    }

    /**
     * DOC sgandon Comment method "getAllBundles".
     * 
     * @param locationAndManifests
     */
    private void computeAllBundles(Map<String, Manifest> locationAndManifests) {
        bundlesMap = new HashMap<>();
        for (Entry<String, Manifest> locAndManifest : locationAndManifests.entrySet()) {
            Manifest manifest = locAndManifest.getValue();
            String bsn = manifest.getMainAttributes().getValue("Bundle-SymbolicName"); //$NON-NLS-1$
            if (bsn != null) {
                int semiColonId = bsn.indexOf(';');
                if (semiColonId != -1) {
                    bsn = bsn.substring(0, semiColonId);
                }
                Bundle bundle = new Bundle(bsn, locAndManifest.getKey());
                bundle.parse(manifest.getMainAttributes());
                bundlesMap.put(bsn, bundle);
            }// else not a OSGI bundle so ignor
        }
    }

    /**
     * return all MANIFEST.MF file found in subfolder of root
     * 
     * @throws IOException
     */
    Map<String, Manifest> getAllManifests() throws IOException {
        Collection<File> allManifestFiles = FileUtils.listFiles(root,
                FileFilterUtils.nameFileFilter("MANIFEST.MF"), TrueFileFilter.INSTANCE); //$NON-NLS-1$
        HashMap<String, Manifest> localtionAndManifest = new HashMap<>();
        for (File manifestF : allManifestFiles) {
            if (manifestF.getAbsolutePath().contains(File.separatorChar + "main" + File.separatorChar)) {
                FileInputStream fis = new FileInputStream(manifestF);
                try {
                    localtionAndManifest.put(manifestF.getParentFile().getParent(), new Manifest(fis));
                } finally {
                    fis.close();
                }
            }
        }
        return localtionAndManifest;
    }

    /**
     * Getter for allBundles.
     * 
     * @return the allBundles
     */
    public Collection<Bundle> getAllBundles() {
        return bundlesMap.values();
    }

    /**
     * Getter for allBundles.
     * 
     * @return the allBundles
     */
    public Map<String, Bundle> getBsnBundlesMap() {
        return bundlesMap;
    }

}
