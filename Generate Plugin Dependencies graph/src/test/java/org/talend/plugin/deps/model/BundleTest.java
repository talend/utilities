// ============================================================================
//
// Copyright (C) 2006-2014 Talend Inc. - www.talend.com
//
// This source code is available under agreement available at
// %InstallDIR%\features\org.talend.rcp.branding.%PRODUCTNAME%\%PRODUCTNAME%license.txt
//
// You should have received a copy of the agreement
// along with this program; if not, write to Talend SA
// 9 rue Pages 92150 Suresnes, France
//
// ============================================================================
package org.talend.plugin.deps.model;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.jar.Manifest;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.talend.plugin.deps.ManifestDependencyParserTest;

/**
 * created by sgandon on 28 juil. 2014 Detailled comment
 *
 */
public class BundleTest {

    static final String BUNDLE_PATH_SUBPATH = "/multirepo-test/repo1/main/plugins/plug11"; //$NON-NLS-1$

    static final File BUNDLE_MANIFEST_PATH = FileUtils.toFile(ManifestDependencyParserTest.class.getResource(BUNDLE_PATH_SUBPATH
            + "/META-INF/MANIFEST.MF")); //$NON-NLS-1$

    @Test
    public void testParse() throws IOException {
        Bundle bundle = createPlug11Bundle();
        String[] requiredBundles = bundle.getRequiredBundles();
        assertNotNull(requiredBundles);
        assertEquals(2, requiredBundles.length);
        assertEquals("plug21", requiredBundles[0]); //$NON-NLS-1$
        assertEquals("plug22", requiredBundles[1]); //$NON-NLS-1$
    }

    public Bundle createPlug11Bundle() throws IOException {
        Bundle bundle = new Bundle("plug11", BUNDLE_MANIFEST_PATH.getParentFile().getParent()); //$NON-NLS-1$
        FileInputStream fis = new FileInputStream(BUNDLE_MANIFEST_PATH);
        try {
            bundle.parse(new Manifest(fis).getMainAttributes());
        } finally {
            fis.close();
        }
        return bundle;
    }
}
