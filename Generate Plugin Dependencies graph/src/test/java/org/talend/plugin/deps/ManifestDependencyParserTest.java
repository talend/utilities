// ============================================================================
//
// Copyright (C) 2006-2014 Talend Inc. - www.talend.com
//
// This source code is available under agreement available at
// %InstallDIR%\features\org.talend.rcp.branding.%PRODUCTNAME%\%PRODUCTNAME%license.txt
//
// You should have received a copy of the agreement
// along with this program; if not, write to Talend SA
// 9 rue Pages 92150 Suresnes, France
//
// ============================================================================
package org.talend.plugin.deps;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.jar.Manifest;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

/**
 * created by sgandon on 28 juil. 2014 Detailled comment
 *
 */
public class ManifestDependencyParserTest {

    static final File MULTI_REPO_TEST_PATH = FileUtils.toFile(ManifestDependencyParserTest.class.getResource("/multirepo-test")); //$NON-NLS-1$

    /**
     * Test method for {@link org.talend.plugin.deps.ManifestDependencyParser#parse()}.
     * 
     * @throws IOException
     */
    @Test
    public void testGetAllManifests() throws IOException {
        ManifestDependencyParser manifestDependencyParser = new ManifestDependencyParser(MULTI_REPO_TEST_PATH);
        Map<String, Manifest> LocationAndManifest = manifestDependencyParser.getAllManifests();
        assertNotNull(LocationAndManifest);
        assertEquals(6, LocationAndManifest.size());
    }

    /**
     * Test method for {@link org.talend.plugin.deps.ManifestDependencyParser#parse()}.
     * 
     * @throws IOException
     */
    @Test
    public void testGetAllBundles() throws IOException {
        ManifestDependencyParser manifestDependencyParser = new ManifestDependencyParser(MULTI_REPO_TEST_PATH);
        manifestDependencyParser.parse();
        assertNotNull(manifestDependencyParser.getAllBundles());
        assertEquals(6, manifestDependencyParser.getAllBundles().size());
    }
}
